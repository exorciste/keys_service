from enum import Enum


class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        return tuple((x.value, x.value) for x in cls)

    @classmethod
    def choice(cls, key):
        choises_dict = {x.name: x.value for x in cls}
        return choises_dict.get(key)
