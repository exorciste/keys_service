import random
import string
import factory

from django.core.management.base import BaseCommand
from factory.fuzzy import BaseFuzzyAttribute


class RandomKeys(BaseFuzzyAttribute):
    def __init__(self, length=4):
        self.length = length

    def fuzz(self):
        symbols = string.ascii_letters + string.digits
        return ''.join(random.choice(symbols) for i in range(self.length))


class KeyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'core.Key'
        django_get_or_create = ('key',)

    key = RandomKeys(length=4)


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            '--count', dest='count', required=True,
            help='Count keys generate',
        )

    def handle(self, *args, **options):
        if options['count']:
            KeyFactory.create_batch(int(options['count']))
