from django.contrib.auth.models import User
from rest_framework import permissions


class IsStaff(permissions.BasePermission):
    """Permissions only POST and PUT methods"""
    def has_permission(self, request, view):
        user = request.user
        if request.method == 'POST' or request.method == 'PUT':
            if user.is_staff is True or user.is_superuser is True:
                return User.objects.filter(id=user.id).exists()
            else:
                return False
        elif request.method == 'GET':
            return True

