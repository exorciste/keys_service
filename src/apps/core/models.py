from django.db import models
from django.utils.translation import ugettext_lazy as _

from src.apps.core.enums import StatusKey


class Key(models.Model):
    key = models.CharField(verbose_name=_('Ключ'), max_length=4, unique=True)
    status = models.CharField(verbose_name=_('Статус'), max_length=50, choices=StatusKey.choices(),
                              default=StatusKey.NEW.value)

    class Meta:
        verbose_name = _("Ключ")
        verbose_name_plural = _("Ключи")
