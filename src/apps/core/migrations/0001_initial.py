# Generated by Django 2.1.5 on 2019-01-28 06:09

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Key',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.CharField(max_length=4, unique=True, verbose_name='Ключ')),
                ('status', models.CharField(choices=[('New', 'New'), ('Released', 'Released'), ('Used', 'Used')], default='New', max_length=50, verbose_name='Статус')),
            ],
            options={
                'verbose_name': 'Ключ',
                'verbose_name_plural': 'Ключи',
            },
        ),
    ]
