from src.apps.common.enums import ChoiceEnum
from django.utils.translation import ugettext_lazy as _


class StatusKey(ChoiceEnum):
    NEW = _('New')
    RELEASED = _('Released')
    USED = _('Used')
