from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient

from src.apps.api.serializers import GetAndSetForKeySerializer, CheckStatusKeySerializer
from src.apps.common.management.commands import KeyFactory
from src.apps.core.enums import StatusKey
from src.apps.core.models import Key


class TestApiKey(TestCase):

    def setUp(self):
        self.count = 10
        self.client = APIClient()
        KeyFactory.create_batch(self.count)
        self.sfaff_user = User.objects.create_user(username='test', password='ZRfmvj87', is_staff=True)
        self.client.login(username='test', password='ZRfmvj87')
        self.random_key = Key.objects.all().first()

    def get_all_keys(self):
        # Getting all keys
        while self.client.get(reverse('get_and_status_key'), format='json').status_code == 200:
            self.client.get(reverse('get_and_status_key'))

    def test_get_key(self):
        response = self.client.get(reverse('get_and_status_key'), format='json')
        new_key = response.json().get('key', False)
        key_object = Key.objects.get(key=new_key)
        serializer = GetAndSetForKeySerializer(key_object)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(key_object.status, StatusKey.choice('RELEASED'))
        self.assertEqual(len(new_key), 4)

    def test_check_status_key(self):
        response = self.client.post(reverse('get_and_status_key'), data={'key': self.random_key.key}, format='json')
        status_key = response.json().get('status', False)
        serializer = CheckStatusKeySerializer(self.random_key, data=response.data)
        if serializer.is_valid():
            self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(status_key, StatusKey.choice('NEW'))
        response = self.client.post(reverse('get_and_status_key'), data={'key': 'Wrong_Key'})
        self.assertEqual(response.status_code, 404)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {"detail": "Не найдено."})

        self.get_all_keys()

        random_key = Key.objects.all().first()
        self.assertEqual(random_key.status, StatusKey.choice('RELEASED'))

    def test_set_used_for_key(self):
        response1 = self.client.put(reverse('set_used_key', args=[self.random_key.key]))
        self.assertEqual(response1.status_code, 400)

        self.get_all_keys()

        response2 = self.client.put(reverse('set_used_key', args=[self.random_key.key]))
        self.assertEqual(response2.status_code, 200)

        key = Key.objects.get(key=self.random_key.key)
        self.assertEqual(key.status, StatusKey.choice('USED'))
        response3 = self.client.put(reverse('set_used_key', args=[self.random_key.key]))
        self.assertEqual(response3.status_code, 400)
        response4 = self.client.put(reverse('set_used_key', args=['wrong_key']))
        self.assertEqual(response4.status_code, 404)
        self.assertJSONEqual(str(response4.content, encoding='utf8'), {"detail": "Не найдено."})

    def test_count_new_keys(self):
        response = self.client.get(reverse('count_new_keys'))
        count = int(response.json().get('count'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(count, self.count)
