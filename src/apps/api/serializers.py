from rest_framework import serializers

from src.apps.core.enums import StatusKey


class CheckStatusKeySerializer(serializers.Serializer):
    key = serializers.CharField(max_length=4)
    status = serializers.CharField(max_length=50, required=False)


class GetAndSetForKeySerializer(serializers.Serializer):
    key = serializers.CharField(max_length=4)

    def update(self, instance, validated_data):
        instance.status = StatusKey.choice('USED')
        instance.save()
        return instance
