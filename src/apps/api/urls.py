from django.urls import path
from src.apps.api.views import GetAndStatusKeyView, SetUsedForKeyView, CountNewKeysView

urlpatterns = [
    path(r'key/', GetAndStatusKeyView.as_view(), name='get_and_status_key'),
    path(r'key/<pk>/', SetUsedForKeyView.as_view(), name='set_used_key'),
    path(r'key-count/', CountNewKeysView.as_view(), name='count_new_keys')
]

