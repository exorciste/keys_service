from django.shortcuts import get_object_or_404
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status

from rest_framework.views import APIView
from rest_framework.response import Response

from src.apps.common.permissions import IsStaff
from src.apps.core.enums import StatusKey
from src.apps.core.models import Key
from src.apps.api.serializers import GetAndSetForKeySerializer, CheckStatusKeySerializer


class GetAndStatusKeyView(APIView):
    http_method_names = ['get', 'post']
    permission_classes = (IsStaff,)

    @swagger_auto_schema(operation_summary='Получение ключей',
                         operation_id='api_get_key',
                         responses={
                             '200': GetAndSetForKeySerializer,
                         })
    def get(self, request):
        key = Key.objects.filter(status=StatusKey.choice('NEW')).first()
        if key:
            serializer = GetAndSetForKeySerializer(key)
            if serializer.data:
                key.status = StatusKey.choice('RELEASED')
                key.save()
                result = serializer.data
            else:
                result = serializer.errors
            return Response(result)
        else:
            return Response({"error": "Keys have ended"}, status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary='Проверка статуса ключа',
                         request_body=CheckStatusKeySerializer(),
                         operation_id='api_check_status_key',
                         responses={
                             '200': CheckStatusKeySerializer,
                         })
    def post(self, request):
        key = request.data.get('key')
        key = get_object_or_404(Key, key=key)
        serializer = CheckStatusKeySerializer(key, data=request.data)
        if serializer.is_valid(raise_exception=True):
            return Response(serializer.data)
        else:
            return Response(serializer.errors)


class SetUsedForKeyView(APIView):
    permission_classes = (IsStaff,)

    @swagger_auto_schema(operation_summary='Поменить ключ как использованный',
                         operation_id='api_set_used_for_key',
                         responses={
                             '200': openapi.Response(
                                 "{success: Key 'key_name' set as used}"
                             ),
                         })
    def put(self, request, pk):
        key = get_object_or_404(Key, key=pk)
        if key.status == StatusKey.choice('NEW'):
            return Response({"error": "Key '{}' not released".format(pk)}, status.HTTP_400_BAD_REQUEST)
        elif key.status == StatusKey.choice('USED'):
            return Response({"error": "Key '{}' already used".format(pk)}, status.HTTP_400_BAD_REQUEST)
        elif key.status == StatusKey.choice('RELEASED'):
            serializer = GetAndSetForKeySerializer(instance=key, data={}, partial=True)
            if serializer.is_valid(raise_exception=True):
                key_saved = serializer.save()
                return Response({"success": "Key '{}' set as used".format(key_saved.key)})
            else:
                return Response(serializer.errors)


class CountNewKeysView(APIView):
    http_method_names = ['get', 'post']

    @swagger_auto_schema(operation_summary='Количество оставшихся ключей',
                         operation_id='api_get_count_new_keys',
                         responses={
                             '200': '{"count": int}',
                         })
    def get(self, request):
        count = Key.objects.filter(status=StatusKey.choice('NEW')).count()
        return Response({"count": "{}".format(count)})
